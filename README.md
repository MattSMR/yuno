# Yuno
A multipurpose Discord bot

## Invite the bot
The bot is currently private as it is still new, with limited features.
Check back later to see if it is public yet.

## Features
- NSFW commands
- Meme commands
- Music playing (Soundcloud, YouTube...)
- Overwatch stats commands

## Coming soon
- League of Legends stats command (API application is being reviewed by Riot)

## Commands
`.help`

- Replies with different commands that the bot understands


`.send help`

- Replies with all of the NSFW commands


`.send memes`

- Sends a random trending meme from either /r/dankmemes, /r/memes, or /r/me_irl


`.send /r/{subreddit}`

- Sends a random trending image from specified subreddit


`.play {search}`

- Searches YouTube for songs


`.play {url}`

- Plays specified URL (supports Soundcloud and YouTube urls)


`.overwatch {battletag}`
  
- Replies with detailed statistics of specific Overwatch user


