package me.mattsmr.yuno.music;

import net.dv8tion.jda.core.entities.User;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SongSelection {

    private User owner;
    private String messageId;
    private List<JSONObject> songs;

    SongSelection(User owner, String messageId) {
        this.owner = owner;
        this.messageId = messageId;
        this.songs = new ArrayList<>();
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public List<JSONObject> getSongs() {
        return songs;
    }

    public void addSong(JSONObject song) {
        this.songs.add(song);
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }
}
