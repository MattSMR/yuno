package me.mattsmr.yuno.music;

import com.sedmelluq.discord.lavaplayer.player.AudioLoadResultHandler;
import com.sedmelluq.discord.lavaplayer.tools.FriendlyException;
import com.sedmelluq.discord.lavaplayer.track.AudioPlaylist;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import me.mattsmr.yuno.Yuno;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.VoiceChannel;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicReference;

public class MusicManager {

    public static List<AudioTrack> queue = new ArrayList<>();

    public static void run(String search, MessageReceivedEvent event) throws IOException, JSONException {
        event.getChannel().sendMessage("**:mag: Searching for** `" + search + "`...").submit();
        VoiceChannel channel;
        List<Member> mentions = new LinkedList<>(event.getMessage().getMentionedMembers());
        for (Member mention : mentions) {
            if (mention.getUser() == Yuno.discord.getSelfUser()) {
                mentions.remove(mention);
                break;
            }
        }
        if (mentions.size() == 1) {
            if (mentions.get(0).getVoiceState().getChannel() == null) {
                event.getChannel().sendMessage(mentions.get(0).getEffectiveName() + " isn't in a voice channel, baka").submit();
                return;
            }
            channel = event.getMessage().getMentionedMembers().get(0).getVoiceState().getChannel();
        } else {
            channel = event.getMember().getVoiceState().getChannel();
        }
        event.getGuild().getAudioManager().openAudioConnection(channel);
        event.getGuild().getAudioManager().setSendingHandler(new AudioPlayerSendHandler(Yuno.player));
        if (search.startsWith("https://")) {
            Yuno.playerManager.loadItem(search,
                    new AudioLoadResultHandler() {
                        @Override
                        public void trackLoaded(AudioTrack track) {
                            queue.add(track);
                            event.getChannel().sendMessage(":notepad_spiral: **Queued:** `" + track.getInfo().title + "`.").submit();
                            if (Yuno.player.getPlayingTrack() == null) {
                                Yuno.player.playTrack(MusicManager.queue.get(0));
                                event.getChannel().sendMessage(":radio_button: **Now playing:** `" + track.getInfo().title + "`.").submit();
                                MusicManager.queue.remove(0);
                            }
                        }

                        @Override
                        public void playlistLoaded(AudioPlaylist playlist) {
                            queue.addAll(playlist.getTracks());
                            event.getChannel().sendMessage(":notepad_spiral: **Queued playlist.**").submit();
                        }

                        @Override
                        public void noMatches() {
                            event.getChannel().sendMessage("i couldn't find anything!! >///<").submit();
                            event.getGuild().getAudioManager().closeAudioConnection();
                        }

                        @Override
                        public void loadFailed(FriendlyException throwable) {
                            event.getChannel().sendMessage("everything just exploded omg what did you do???").submit();
                            event.getGuild().getAudioManager().closeAudioConnection();
                        }
                    }
            );
        } else {
            Document document = Jsoup.connect("https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=5&type=video&order=relevance&q=" + search.replaceAll(" ", "+")
                    + "&key=AIzaSyCdfCrGCIaWZ9iB6AlQ3WDsRZliY82Hy9A")
                    .ignoreContentType(true)
                    .timeout(10000).get();
            String json = document.text();
            JSONObject jsonObject = (JSONObject) new JSONTokener(json).nextValue();
            if (jsonObject.getJSONArray("items").length() == 0) {
                event.getChannel().sendMessage("i couldn't find anything!! >///<").submit();
                return;
            }
            SongSelection selection;
            if (event.getMessage().getMentionedUsers().size() == 1) {
                selection = new SongSelection(event.getMessage().getMentionedUsers().get(0), event.getMessageId());
            } else {
                selection = new SongSelection(event.getMessage().getAuthor(), event.getMessageId());
            }
            EmbedBuilder embedBuilder = new EmbedBuilder();
            embedBuilder.setAuthor("Searched by " + event.getAuthor().getName(), event.getAuthor().getAvatarUrl(), event.getAuthor().getAvatarUrl());
            embedBuilder.setTitle("Search results for \"" + search + "\":");
            char[] alphabet = "ABCDE".toCharArray();
            for (int i = 0; i < jsonObject.getJSONArray("items").length(); i++) {
                embedBuilder.appendDescription("**" + alphabet[i] + ":** " + jsonObject.getJSONArray("items").getJSONObject(i).getJSONObject("snippet").getString("title") + "\n");
                selection.addSong(jsonObject.getJSONArray("items").getJSONObject(i));
            }
            event.getChannel().sendMessage(embedBuilder.build()).queue(message -> {
                try {
                    if (jsonObject.getJSONArray("items").length() >= 1) {
                        message.addReaction("\uD83C\uDDE6").queue();
                    }
                    if (jsonObject.getJSONArray("items").length() >= 2) {
                        message.addReaction("\uD83C\uDDE7").queue();
                    }
                    if (jsonObject.getJSONArray("items").length() >= 3) {
                        message.addReaction("\uD83C\uDDE8").queue();
                    }
                    if (jsonObject.getJSONArray("items").length() >= 4) {
                        message.addReaction("\uD83C\uDDE9").queue();
                    }
                    if (jsonObject.getJSONArray("items").length() >= 5) {
                        message.addReaction("\uD83C\uDDEA").queue();
                    }
                } catch (JSONException ex) {
                    ex.printStackTrace();
                }
                Yuno.selections.put(message.getId(), selection);
            });
        }

    }

    public static AudioTrack getTrackFromIdentifier(String search) {
        AtomicReference<AudioTrack> atomicReference = new AtomicReference<>(null);
        try {
            Yuno.playerManager.loadItem(search,
                    new AudioLoadResultHandler() {
                        @Override
                        public void trackLoaded(AudioTrack track) {
                            atomicReference.set(track);
                        }

                        @Override
                        public void playlistLoaded(AudioPlaylist playlist) {
                        }

                        @Override
                        public void noMatches() {
                        }

                        @Override
                        public void loadFailed(FriendlyException throwable) {
                        }
                    }
            ).get();
        } catch (InterruptedException | ExecutionException ex) {
            ex.printStackTrace();
        }
        if (atomicReference.get() == null) {
            System.out.println("track is null");
            return null;
        }
        return atomicReference.get();
    }
}
