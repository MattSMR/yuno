package me.mattsmr.yuno.karaoke;

import net.dv8tion.jda.core.entities.Message;

import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicInteger;

public class KaraokeData {

    private HashMap<Integer, String> lyrics = new HashMap<>();
    private Message message;

    public void load(Song song) {
        switch (song) {
            case SONO_CHI_NO_SADAME:
                lyrics.clear();
                lyrics.put(11, "Sora koboreochita futatsu no hoshi ga");
                lyrics.put(17, "Hikari to yami no minamo suikomareteyuku");
                lyrics.put(23, "Hikiau you ni kasanaru hamon");
                lyrics.put(33, "Hokori no michi wo yuku mono ni taiyou no michibiki wo");
                lyrics.put(40, "Yabou no hate wo mezasu mono ni");
                lyrics.put(46, "ikenie wo");
                lyrics.put(53, "Furueru hodo kokoro moetsukiru hodo atsuku");
                lyrics.put(58, "Sono te kara hanate kodou");
                lyrics.put(62, "karada minagiru yuuki de");
                lyrics.put(65, "Mayoi naki kakugo ni 'Kassai' wo");
                lyrics.put(72, "Sono chi no sadame");
                lyrics.put(76, "JOJO!");
                break;
        }
    }

    public void start() {
        final int length = 120;
        AtomicInteger time = new AtomicInteger(0);
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                time.addAndGet(1);
                if (lyrics.containsKey(time.get())) {
                    message.editMessage(lyrics.get(time.get())).submit();
                }
            }
        }, 0, 1000);
    }

    public enum Song {
        SONO_CHI_NO_SADAME
    }

}
