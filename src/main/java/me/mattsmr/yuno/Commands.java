package me.mattsmr.yuno;

import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import me.mattsmr.yuno.music.MusicManager;
import me.mattsmr.yuno.utils.Playlist;
import net.dean.jraw.models.Listing;
import net.dean.jraw.models.Submission;
import net.dean.jraw.pagination.DefaultPaginator;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.kodehawa.lib.imageboards.DefaultImageBoards;
import net.kodehawa.lib.imageboards.entities.BoardImage;
import org.json.JSONException;
import org.json.JSONObject;
import pw.aru.api.nekos4j.Nekos4J;
import pw.aru.api.nekos4j.image.Image;
import pw.aru.api.nekos4j.image.ImageProvider;

import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ThreadLocalRandom;

import static me.mattsmr.yuno.Yuno.player;

public class Commands {

    public void help(String[] args, MessageReceivedEvent event) {
        EmbedBuilder embedBuilder = new EmbedBuilder();
        embedBuilder.setTitle("Help");
        embedBuilder.setDescription("help, send memes, play <song name/url>, skip, stop, send help");
        embedBuilder.setAuthor("Yuno");
        embedBuilder.setColor(Color.GREEN);
        event.getChannel().sendMessage(embedBuilder.build()).submit();
    }

    public void send(String[] args, MessageReceivedEvent event) {
        if (args.length == 0) {
            event.getChannel().sendMessage("you actually have to tell me what you want im not magic").submit();
            return;
        }
        String subreddit;
        if (args[0].equalsIgnoreCase("real")) {
            subreddit = "nsfw";
        } else if (args[0].equalsIgnoreCase("trap") || args[1].equalsIgnoreCase("futa")) {
            event.getChannel().sendMessage("ha gay").submit();
            return;
        } else if (args[0].equalsIgnoreCase("hentai")) {
            subreddit = "hentai";
        } else if (args[0].equalsIgnoreCase("yuri")) {
            subreddit = "yuri";
        } else if (args[0].equalsIgnoreCase("overwatch")) {
            subreddit = "Overwatch_Porn";
        } else if (args[0].equalsIgnoreCase("my") && args[1].equalsIgnoreCase("hero") && args[2].equalsIgnoreCase("academia")) {
            subreddit = "BokuNoEroAcademia";
        } else if (args[0].equalsIgnoreCase("neko")) {
            if (ThreadLocalRandom.current().nextBoolean()) {
                if (!event.getTextChannel().isNSFW()) {
                    event.getChannel().sendMessage("THIS IS A CHRISTIAN SERVER\nNO LEWD NEKOS SHALL BE SHOWN HERE").submit();
                    return;
                }
                Nekos4J api = new Nekos4J.Builder().build();
                ImageProvider imageProvider = api.getImageProvider();
                Image image = imageProvider.getRandomImage("lewd").execute();
                EmbedBuilder embedBuilder = new EmbedBuilder();
                embedBuilder.setImage(image.getUrl());
                embedBuilder.setTitle("Nekos");
                embedBuilder.setFooter("Nekos provided by nekos.life", null);
                embedBuilder.setColor(ThreadLocalRandom.current().nextInt());
                event.getChannel().sendMessage(embedBuilder.build()).submit();
                return;
            }
            subreddit = "Nekomimi";
        } else if (args[0].equalsIgnoreCase("monster")) {
            subreddit = "monstergirl";
        } else if (args[0].equalsIgnoreCase("2b")) {
            subreddit = "2Booty";
        } else if (args[0].equalsIgnoreCase("league")) {
            subreddit = "rule34lol";
        } else if (args[0].equalsIgnoreCase("cats")) {
            subreddit = "cats";
        } else if (args[0].equalsIgnoreCase("meme")) {
            int random = ThreadLocalRandom.current().nextInt(3);
            switch (random) {
                case 0:
                    subreddit = "meirl";
                    break;
                case 1:
                    subreddit = "2meirl4meirl";
                    break;
                case 2:
                    subreddit = "dankmemes";
                    break;
                default:
                    return;
            }
        } else if (args[0].equalsIgnoreCase("help")) {
            event.getChannel().sendMessage(":eggplant: **SENDING HELP**" +
                    "\n**My collection contains:**"
                    + "\n - real porn"
                    + "\n - hentai"
                    + "\n - yuri"
                    + "\n - overwatch"
                    + "\n - my hero academia"
                    + "\n - neko"
                    + "\n - 2b"
                    + "\n - monster girl"
                    + "\n - league"
            ).submit();
            return;
        } else if (args[0].startsWith("/r/")) {
            subreddit = args[0].split("/r/")[1];
        } else {
            event.getChannel().sendMessage("idk what that is").submit();
            return;
        }
        EmbedBuilder builder = new EmbedBuilder();
        if (subreddit.equalsIgnoreCase("cats")) {
            if (ThreadLocalRandom.current().nextInt(2) == 1) {
                builder.setImage("https://cataas.com/cat");
                builder.setTitle("Random cat photo.");
                builder.setDescription("from cataas.com");
                event.getChannel().sendMessage(builder.build()).submit();
                return;
            }
        }
        DefaultPaginator<Submission> paginator = Yuno.reddit
                .subreddit(subreddit)
                .posts()
                .limit(50)
                .build();
        java.util.List<Listing<Submission>> pages;
        try {
            pages = paginator.accumulate(1);
        } catch (NullPointerException ex) {
            event.getChannel().sendMessage("subreddit doesnt exist like matts friends hahahaha please help").submit();
            return;
        }
        List<Submission> posts = new ArrayList<>();
        pages.get(0).forEach(submission -> {
            if (!submission.isSelfPost() && !submission.getUrl().contains("imgur.com/a/")) {
                posts.add(submission);
            }
        });
        Submission submission = posts.get(ThreadLocalRandom.current().nextInt(posts.size() + 1));
        if (submission.isNsfw() && !event.getTextChannel().isNSFW()) {
            event.getChannel().sendMessage("THIS IS A CHRISTIAN SERVER\nNO LEWD CONTENT HERE").submit();
            return;
        }
        String thumbnail = submission.getUrl();
        if (thumbnail.contains("gfycat")) {
            thumbnail = "https://thumbs." + thumbnail.split("https://")[1] + "-size_restricted.gif";
        } else if (!thumbnail.endsWith(".png") && !thumbnail.endsWith(".jpg") && !thumbnail.endsWith(".jpeg") && !thumbnail.endsWith(".gif")
                && thumbnail.contains("imgur.com")) {
            thumbnail = "https://i." + thumbnail.split("https://")[1] + ".png";
        }
        builder.setImage(thumbnail);
        builder.setTitle(submission.getTitle());
        builder.setDescription("from /r/" + subreddit);
        builder.setFooter("\uD83D\uDC4D " + submission.getVote().name() + " | \uD83D\uDCAC " + submission.getCommentCount(), null);
        event.getChannel().sendMessage(builder.build()).submit();
    }

    public void catface(String[] args, MessageReceivedEvent event) {
        EmbedBuilder builder = new EmbedBuilder();
        builder.setColor(ThreadLocalRandom.current().nextInt());
        try {
            builder.setDescription(Unirest.get("https://catfact.ninja/fact").asJson().getBody().getObject().getString("fact"));
        } catch (JSONException | UnirestException ex) {
            ex.printStackTrace();
        }
        builder.setTitle("Cat fact:");
        builder.setAuthor("Requested by " + event.getAuthor().getName(), event.getAuthor().getAvatarUrl(), event.getAuthor().getAvatarUrl());
        event.getChannel().sendMessage(builder.build()).submit();
    }

    public void avatar(String[] args, MessageReceivedEvent event) {
        if (event.getMessage().getMentionedUsers().size() != 1) {
            event.getChannel().sendMessage("yeah, but like who?").submit();
        }
        EmbedBuilder builder = new EmbedBuilder();
        builder.setColor(ThreadLocalRandom.current().nextInt());
        builder.setTitle(event.getMessage().getMentionedUsers().get(0).getName() + "'s avatar:");
        builder.setImage(event.getMessage().getMentionedUsers().get(0).getAvatarUrl());
        event.getChannel().sendMessage(builder.build()).submit();
    }

    public void ily(String[] args, MessageReceivedEvent event) {
        if (event.getAuthor().getId().equals("189150146380955649")) {
            event.getChannel().sendMessage("love you too, habin!").submit();
            return;
        }
        event.getChannel().sendMessage("i only love habin.").submit();
    }

    public void playlist(String[] args, MessageReceivedEvent event) {
        if (args[0].equalsIgnoreCase("queue")) {
            event.getChannel().sendMessage(":notepad_spiral: Added all songs from queue to playlist.").submit();
            MusicManager.queue.forEach(track -> Playlist.add(event.getAuthor(), track));
        } else if (args[0].equalsIgnoreCase("play")) {
            User author;
            List<User> mentions = new ArrayList<>(event.getMessage().getMentionedUsers());
            mentions.remove(Yuno.discord.getSelfUser());
            author = mentions.size() == 1 ? mentions.get(0) : event.getAuthor();
            if (Playlist.get(author).size() == 0) {
                event.getChannel().sendMessage("that playlist doesnt even exist smh").submit();
                return;
            }
            event.getChannel().sendMessage(":musical_note: Queued your playlist.").submit();
            MusicManager.queue.addAll(Playlist.get(author));
            try {
                MusicManager.run("https://www.youtube.com/watch?v=" + Playlist.get(author).get(0).getIdentifier(), event);
                MusicManager.queue.remove(Playlist.get(author).get(0));
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        } else if (args[0].equalsIgnoreCase("add")) {
            Playlist.add(event.getAuthor(), player.getPlayingTrack());
            event.getChannel().sendMessage(":musical_note: Added " + player.getPlayingTrack().getInfo().title + " your playlist.").submit();
        } else if (args[0].equalsIgnoreCase("reset")) {
            Playlist.reset(event.getAuthor());
            event.getChannel().sendMessage(":musical_note: Reset your playlist.").submit();
        } else if (args[0].equalsIgnoreCase("list")) {
            StringBuilder message = new StringBuilder(":musical_note: **" + event.getAuthor().getName() + "'s Playlist**");
            Playlist.get(event.getAuthor()).forEach(track -> message.append("\n - ").append(track.getInfo().title));
            event.getChannel().sendMessage(message.toString()).submit();
        } else {
            event.getChannel().sendMessage("use `.playlist add`, `.playlist list`, `.playlist play` or `.playlist queue`").submit();
        }
    }

    public void xkcd(String[] args, MessageReceivedEvent event) {
        EmbedBuilder embedBuilder = new EmbedBuilder();
        JSONObject object = null;
        try {
            object = Unirest.get("https://xkcd.com/" + (ThreadLocalRandom.current().nextInt(2033) + 1) + "/info.0.json").asJson().getBody().getObject();
        } catch (UnirestException ex) {
            ex.printStackTrace();
        }
        if (object == null) {
            event.getChannel().sendMessage("everything just exploded omg what did you do?!?").submit();
            return;
        }
        embedBuilder.setTitle(object.getString("title"));
        embedBuilder.setImage(object.getString("img"));
        event.getChannel().sendMessage(embedBuilder.build()).submit();
    }

    public void play(String[] args, MessageReceivedEvent event) {
        if (args.length == 0) {
            event.getChannel().sendMessage("what do i play? its not that hard just tell me").submit();
            return;
        }
        if (event.getMember().getVoiceState().getChannel() == null && event.getMessage().getMentionedMembers().size() != 1) {
            event.getChannel().sendMessage("you aren't in a voice channel, baka").submit();
            return;
        }
        StringBuilder searchBuilder = new StringBuilder();
        for (String word : args) {
            searchBuilder.append(word).append(" ");
        }
        String search = searchBuilder.toString();
        if (event.getMessage().getMentionedUsers().size() > 0) {
            for (Member member : event.getMessage().getMentionedMembers()) {
                search = search.replaceFirst("@" + member.getEffectiveName(), "");
            }
        }
        try {
            MusicManager.run(search, event);
        } catch (IOException | JSONException ex) {
            ex.printStackTrace();
        }
    }

    public void radio(String[] args, MessageReceivedEvent event) {
        if (event.getMember().getVoiceState().getChannel() == null && event.getMessage().getMentionedMembers().size() != 1) {
            event.getChannel().sendMessage("you aren't in a voice channel, baka").submit();
            return;
        }
        String search;
        if (args[0].equalsIgnoreCase("jpop")) {
            search = "https://listen.moe/stream";
        } else if (args[0].equalsIgnoreCase("kpop")) {
            search = "https://listen.moe/kpop/stream";
        } else {
            event.getChannel().sendMessage("use `.radio kpop` or `.radio jpop`.").submit();
            return;
        }
        try {
            MusicManager.run(search, event);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public void stop(String[] args, MessageReceivedEvent event) {
        event.getChannel().sendMessage("wow rude!\nis my music not good enough??").submit();
        event.getGuild().getAudioManager().closeAudioConnection();
        MusicManager.queue.clear();
    }

    public void trivia(String[] args, MessageReceivedEvent event) {
        event.getChannel().sendMessage("Starting trivia game...").submit();
        EmbedBuilder embedBuilder = new EmbedBuilder();
        embedBuilder.setTitle("Trivia");
        embedBuilder.setDescription("Pick a category:"
                + "\n\n - :video_game: Video games"
                + "\n\n - :clapper: TV shows/movies"
                + "\n\n - :flag_jp: Anime/manga"
                + "\n\n - :computer: Computer science"
                + "\n\n - :thinking: General knowledge"
                + "\n\n - :twisted_rightwards_arrows: Random"
        );
        embedBuilder.setFooter("Started by " + event.getAuthor().getName(), event.getAuthor().getAvatarUrl());
        try {
            Message message = event.getChannel().sendMessage(embedBuilder.build()).submit().get();
            message.addReaction("\uD83C\uDFAE").queue();
            message.addReaction("\uD83C\uDFAC").queue();
            message.addReaction("\uD83C\uDDEF\uD83C\uDDF5").queue();
            message.addReaction("\uD83D\uDCBB").queue();
            message.addReaction("\uD83E\uDD14").queue();
            message.addReaction("\uD83D\uDD00").queue();
            Yuno.trivias.put(message.getId(), message);
        } catch (InterruptedException | ExecutionException ex) {
            ex.printStackTrace();
        }
    }

    public void np(String[] args, MessageReceivedEvent event) {
        event.getChannel().sendMessage(":musical_keyboard: **Currently playing:** \"" + player.getPlayingTrack().getInfo().title + "\"").submit();
    }

    public void r34(String[] args, MessageReceivedEvent event) {
        if (!event.getTextChannel().isNSFW()) {
            event.getChannel().sendMessage("THE POWER OF CHRIST COMPELS YOU" +
                    "\nNO FURRY PORN HERE!!").submit();
            return;
        }
        if (args.length == 0) {
            event.getChannel().sendMessage("what shall i lewd?").submit();
            return;
        }
        StringBuilder searchBuilder = new StringBuilder();
        for (String word : args) {
            searchBuilder.append(word).append(" ");
        }
        String search = searchBuilder.toString();
        if (DefaultImageBoards.RULE34.search(search.replaceAll(" ", "_")).blocking().size() == 0) {
            event.getChannel().sendMessage("no results found, no porn for you").submit();
            return;
        }
        BoardImage image = DefaultImageBoards.RULE34.search(search.replaceAll(" ", "_")).blocking().get(0);
        EmbedBuilder embedBuilder = new EmbedBuilder();
        embedBuilder.setTitle("Rule 34:");
        embedBuilder.setColor(ThreadLocalRandom.current().nextInt());
        embedBuilder.setFooter("Requested by " + event.getAuthor().getName(), event.getAuthor().getAvatarUrl());
        embedBuilder.setImage(image.getURL());
        event.getChannel().sendMessage(embedBuilder.build()).submit();
    }

    public void yandere(String[] args, MessageReceivedEvent event) {
        if (!event.getTextChannel().isNSFW()) {
            event.getChannel().sendMessage("THE POWER OF CHRIST COMPELS YOU" +
                    "\nNO HENTAI HERE!!").submit();
            return;
        }
        if (args.length == 0) {
            event.getChannel().sendMessage("what shall i lewd?").submit();
            return;
        }
        StringBuilder searchBuilder = new StringBuilder();
        for (String word : args) {
            searchBuilder.append(word).append(" ");
        }
        String search = searchBuilder.toString();
        if (DefaultImageBoards.YANDERE.search(search.replaceAll(" ", "_")).blocking().size() == 0) {
            event.getChannel().sendMessage("no results found, no porn for you").submit();
            return;
        }
        BoardImage image = DefaultImageBoards.YANDERE.search(search.replaceAll(" ", "_")).blocking().get(0);
        EmbedBuilder embedBuilder = new EmbedBuilder();
        embedBuilder.setTitle("Yande.re:");
        embedBuilder.setColor(ThreadLocalRandom.current().nextInt());
        embedBuilder.setFooter("Requested by " + event.getAuthor().getName(), event.getAuthor().getAvatarUrl());
        embedBuilder.setImage(image.getURL());
        event.getChannel().sendMessage(embedBuilder.build()).submit();
    }

    public void danbooru(String[] args, MessageReceivedEvent event) {
        if (!event.getTextChannel().isNSFW()) {
            event.getChannel().sendMessage("THE POWER OF CHRIST COMPELS YOU" +
                    "\nNO HENTAI HERE!!").submit();
            return;
        }
        if (args.length == 0) {
            event.getChannel().sendMessage("what shall i lewd?").submit();
            return;
        }
        StringBuilder searchBuilder = new StringBuilder();
        for (String word : args) {
            searchBuilder.append(word).append(" ");
        }
        String search = searchBuilder.toString();
        if (DefaultImageBoards.DANBOORU.search(search.replaceAll(" ", "_")).blocking().size() == 0) {
            event.getChannel().sendMessage("no results found, no porn for you").submit();
            return;
        }
        BoardImage image = DefaultImageBoards.DANBOORU.search(search.replaceAll(" ", "_")).blocking().get(0);
        EmbedBuilder embedBuilder = new EmbedBuilder();
        embedBuilder.setTitle("Danbooru:");
        embedBuilder.setColor(ThreadLocalRandom.current().nextInt());
        embedBuilder.setFooter("Requested by " + event.getAuthor().getName(), event.getAuthor().getAvatarUrl());
        embedBuilder.setImage(image.getURL());
        event.getChannel().sendMessage(embedBuilder.build()).submit();
    }

    public void e621(String[] args, MessageReceivedEvent event) {
        if (!event.getTextChannel().isNSFW()) {
            event.getChannel().sendMessage("THE POWER OF CHRIST COMPELS YOU" +
                    "\nNO HENTAI HERE!!").submit();
            return;
        }
        if (args.length == 0) {
            event.getChannel().sendMessage("what shall i lewd?").submit();
            return;
        }
        StringBuilder searchBuilder = new StringBuilder();
        for (String word : args) {
            searchBuilder.append(word).append(" ");
        }
        String search = searchBuilder.toString();
        if (DefaultImageBoards.E621.search(search.replaceAll(" ", "_")).blocking().size() == 0) {
            event.getChannel().sendMessage("no results found, no porn for you").submit();
            return;
        }
        BoardImage image = DefaultImageBoards.E621.search(search.replaceAll(" ", "_")).blocking().get(0);
        EmbedBuilder embedBuilder = new EmbedBuilder();
        embedBuilder.setTitle("E621:");
        embedBuilder.setColor(ThreadLocalRandom.current().nextInt());
        embedBuilder.setFooter("Requested by " + event.getAuthor().getName(), event.getAuthor().getAvatarUrl());
        embedBuilder.setImage(image.getURL());
        event.getChannel().sendMessage(embedBuilder.build()).submit();
    }

    public void konachan(String[] args, MessageReceivedEvent event) {
        if (!event.getTextChannel().isNSFW()) {
            event.getChannel().sendMessage("THE POWER OF CHRIST COMPELS YOU" +
                    "\nNO HENTAI HERE!!").submit();
            return;
        }
        if (args.length == 0) {
            event.getChannel().sendMessage("what shall i lewd?").submit();
            return;
        }
        StringBuilder searchBuilder = new StringBuilder();
        for (String word : args) {
            searchBuilder.append(word).append(" ");
        }
        String search = searchBuilder.toString();
        if (DefaultImageBoards.KONACHAN.search(search.replaceAll(" ", "_")).blocking().size() == 0) {
            event.getChannel().sendMessage("no results found, no porn for you").submit();
            return;
        }
        BoardImage image = DefaultImageBoards.KONACHAN.search(search.replaceAll(" ", "_")).blocking().get(0);
        EmbedBuilder embedBuilder = new EmbedBuilder();
        embedBuilder.setTitle("Konachan:");
        embedBuilder.setColor(ThreadLocalRandom.current().nextInt());
        embedBuilder.setFooter("Requested by " + event.getAuthor().getName(), event.getAuthor().getAvatarUrl());
        embedBuilder.setImage(image.getURL());
        event.getChannel().sendMessage(embedBuilder.build()).submit();
    }

    public void safebooru(String[] args, MessageReceivedEvent event) {
        if (args.length == 0) {
            event.getChannel().sendMessage("what shall i lewd?").submit();
            return;
        }
        StringBuilder searchBuilder = new StringBuilder();
        for (String word : args) {
            searchBuilder.append(word).append(" ");
        }
        String search = searchBuilder.toString();
        if (DefaultImageBoards.SAFEBOORU.search(search.replaceAll(" ", "_")).blocking().size() == 0) {
            event.getChannel().sendMessage("no results found, no porn for you").submit();
            return;
        }
        BoardImage image = DefaultImageBoards.SAFEBOORU.search(search.replaceAll(" ", "_")).blocking().get(0);
        EmbedBuilder embedBuilder = new EmbedBuilder();
        embedBuilder.setTitle("Safebooru:");
        embedBuilder.setColor(ThreadLocalRandom.current().nextInt());
        embedBuilder.setFooter("Requested by " + event.getAuthor().getName(), event.getAuthor().getAvatarUrl());
        embedBuilder.setImage(image.getURL());
        event.getChannel().sendMessage(embedBuilder.build()).submit();
    }

    public void queue(String[] args, MessageReceivedEvent event) {
        StringBuilder message = new StringBuilder(":musical_note: **Song Queue**");
        MusicManager.queue.forEach(track -> message.append("\n - ").append(track.getInfo().title));
        event.getChannel().sendMessage(message.toString()).submit();
    }

    public void skip(String[] args, MessageReceivedEvent event) {
        Yuno.player.stopTrack();
        if (MusicManager.queue.size() > 0) {
            Yuno.player.playTrack(MusicManager.queue.get(0));
            MusicManager.queue.remove(0);
        }
        if (MusicManager.queue.size() == 0) {
            event.getChannel().sendMessage(":musical_keyboard: Finished queue.").submit();
            return;
        }
        event.getChannel().sendMessage(":musical_keyboard: **Now playing:** `" + player.getPlayingTrack().getInfo().title + "`.").submit();
    }

    public void overwatch(String[] args, MessageReceivedEvent event) {
        if (args.length == 0) {
            event.getChannel().sendMessage("what? who? do like `.overwatch Cow#11150` or something").submit();
            return;
        }
        EmbedBuilder embedBuilder = new EmbedBuilder();
        JSONObject userObject;
        String user = args[1];
        embedBuilder.setTitle(user + "'s Overwatch stats:");
        try {
            userObject = Unirest.get("https://ow-api.com/v1/stats/pc/asia/" + user.replaceFirst("#", "-") + "/profile").asJson().getBody().getObject();
        } catch (UnirestException ex) {
            ex.printStackTrace();
            return;
        }
        try {
            embedBuilder.setAuthor(user, "https://overwatchtracker.com/profile/pc/global/" + user.replaceFirst("#", "-"), userObject.getString("icon"));
            embedBuilder.setDescription("Level: **" + (userObject.getInt("level") + userObject.getInt("prestige") * 100) + "**");
            if (userObject.getInt("gamesWon") == 0) {
                embedBuilder.appendDescription("\n\n:lock: **This profile is private.**\n");
                embedBuilder.setColor(ThreadLocalRandom.current().nextInt());
            } else {
                embedBuilder.appendDescription("\n\n**Games Won**: " + userObject.getInt("gamesWon"));
                if (userObject.getString("rating").equalsIgnoreCase("")) {
                    embedBuilder.appendDescription("\n**SR**: N/A");
                } else {
                    embedBuilder.appendDescription("\n**SR**: " + userObject.getString("rating"));
                }
                embedBuilder.appendDescription("\n\n**Quick Play Stats:**");
                embedBuilder.appendDescription("\n**Avg. Eliminations**: " + userObject.getJSONObject("quickPlayStats").getInt("eliminationsAvg"));
                embedBuilder.appendDescription("\n**.Avg. Damage Done**: " + userObject.getJSONObject("quickPlayStats").getInt("damageDoneAvg"));
                embedBuilder.appendDescription("\n**Avg. Healing Done**: " + userObject.getJSONObject("quickPlayStats").getInt("healingDoneAvg"));
                embedBuilder.appendDescription("\n**Avg. Deaths**: " + userObject.getJSONObject("quickPlayStats").getInt("deathsAvg"));
                embedBuilder.appendDescription("\n\n**Competitive Stats:**");
                embedBuilder.appendDescription("\n**Avg. Eliminations**: " + userObject.getJSONObject("competitiveStats").getInt("eliminationsAvg"));
                embedBuilder.appendDescription("\n**Avg. Damage Done**: " + userObject.getJSONObject("competitiveStats").getInt("damageDoneAvg"));
                embedBuilder.appendDescription("\n**Avg. Healing Done**: " + userObject.getJSONObject("competitiveStats").getInt("healingDoneAvg"));
                embedBuilder.appendDescription("\n**Avg. Deaths**: " + userObject.getJSONObject("competitiveStats").getInt("deathsAvg"));
                embedBuilder.setColor(ThreadLocalRandom.current().nextInt());
            }
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
        event.getChannel().sendMessage(embedBuilder.build()).submit();
    }

    public void anime(String[] args, MessageReceivedEvent event) {
        if (args.length == 0) {
            event.getChannel().sendMessage("which anime shall i search for?").submit();
            return;
        }
        StringBuilder searchBuilder = new StringBuilder();
        for (String word : args) {
            searchBuilder.append(word).append(" ");
        }
        String search = searchBuilder.toString();
        String url = "https://api.jikan.moe/search/anime/" + search + "/1";
        JSONObject object;
        try {
            object = Unirest.get(url).asJson().getBody().getObject();
        } catch (UnirestException ex) {
            ex.printStackTrace();
            return;
        }
        object.getJSONArray("result").getJSONObject(0);
        EmbedBuilder embedBuilder = new EmbedBuilder();
        embedBuilder.setThumbnail(object.getString("image_url"));
        embedBuilder.setColor(ThreadLocalRandom.current().nextInt());
        embedBuilder.setDescription(object.getString("description"));
        embedBuilder.setTitle(object.getString("title"));
        embedBuilder.setFooter("⭐ " + object.getDouble("score"), null);
        event.getChannel().sendMessage(embedBuilder.build()).submit();
    }

    public void ratewaifu(String[] args, MessageReceivedEvent event) {
        EmbedBuilder embedBuilder = new EmbedBuilder();
        String hash;
        String rating;
        String description;
        StringBuilder searchBuilder = new StringBuilder();
        for (String word : args) {
            searchBuilder.append(word).append(" ");
        }
        String search = searchBuilder.toString();
        if (event.getMessage().getMentionedMembers().size() == 0) {
            if (args.length == 0) {
                event.getChannel().sendMessage("choose something for me to rate").submit();
                return;
            }
            hash = String.valueOf(search.hashCode() * 3);
            embedBuilder.setTitle(search + "'s rating:");
            description = search;
        } else {
            hash = event.getMessage().getMentionedUsers().get(0).getId();
            embedBuilder.setTitle(event.getMessage().getMentionedUsers().get(0).getName() + "'s rating:");
            embedBuilder.setThumbnail(event.getMessage().getMentionedUsers().get(0).getAvatarUrl());
            description = event.getMessage().getMentionedUsers().get(0).getAsMention();
        }
        rating = hash.charAt(2) + "." + hash.charAt(3) + hash.charAt(4);
        embedBuilder.setFooter("this is $100% factual information", null);
        embedBuilder.setColor(ThreadLocalRandom.current().nextInt());
        embedBuilder.setDescription(description + " is a " + rating + "/10");
        event.getChannel().sendMessage(embedBuilder.build()).submit();
    }

    public void shuffle(String[] args, MessageReceivedEvent event) {
        Collections.shuffle(MusicManager.queue);
        event.getChannel().sendMessage(":twisted_rightwards_arrows: Shuffled song queue.").submit();
    }

    public void pause(String[] args, MessageReceivedEvent event) {
        player.setPaused(true);
        event.getChannel().sendMessage(":pause_button: Paused music.").submit();
    }

    public void resume(String[] args, MessageReceivedEvent event) {
        if (!player.isPaused()) {
            event.getChannel().sendMessage("nothing is paused").submit();
            return;
        }
        player.setPaused(false);
        event.getChannel().sendMessage(":arrow_forward: Resumed music.").submit();
    }

    public void say(String[] args, MessageReceivedEvent event) {
        if (args.length == 0) {
            event.getChannel().sendMessage("what will you force me to say").submit();
            return;
        }
        StringBuilder sayBuilder = new StringBuilder();
        for (String word : args) {
            sayBuilder.append(word).append(" ");
        }
        String say = sayBuilder.toString();
        event.getChannel().sendMessage(say).submit();
    }

    public void _8ball(String[] args, MessageReceivedEvent event) {
        if (args.length == 0) {
            event.getChannel().sendMessage("what would you like to know?").submit();
            return;
        }
        String answer;
        int random = ThreadLocalRandom.current().nextInt(9);
        switch (random) {
            case 0:
                answer = "Reply hazy. Ask again later.";
                break;
            case 1:
                answer = "cant be assed to answer that one";
                break;
            case 2:
                answer = "I won't answer that.";
                break;
            case 3:
                answer = "Oh, definitely";
                break;
            case 4:
                answer = "Well, yeah...";
                break;
            case 5:
                answer = "Most likely.";
                break;
            case 6:
                answer = "Probably not.";
                break;
            case 7:
                answer = "Why would it?";
                break;
            case 8:
                answer = "Definitely not.";
                break;
            default:
                answer = "Uh... maybe?";
                break;
        }
        EmbedBuilder embedBuilder = new EmbedBuilder();
        embedBuilder.setTitle("Magic 8 Ball");
        StringBuilder searchBuilder = new StringBuilder();
        for (String word : args) {
            searchBuilder.append(word).append(" ");
        }
        String search = searchBuilder.toString();
        embedBuilder.setDescription("**:question: " + event.getMessage().getAuthor().getAsMention() + " asks: **`" + search + "`..." +
                "\n\n**:8ball: 8ball says: **`" + answer + "`");
        embedBuilder.setColor(ThreadLocalRandom.current().nextInt());
        event.getChannel().sendMessage(embedBuilder.build()).submit();
    }

    public void flip(String[] args, MessageReceivedEvent event) {
        event.getChannel().sendMessage(":moneybag: The coin landed on **" + (ThreadLocalRandom.current().nextBoolean() ? "heads" : "tails") + "**.").submit();
    }

    public void roll(String[] args, MessageReceivedEvent event) {
        event.getChannel().sendMessage(":game_die: The die landed on a " + (ThreadLocalRandom.current().nextInt(5) + 1) + ".").submit();
    }



    /*
    * ----------------------------------------------------------------------------
    *
    *                         HOW TO ADD A NEW COMMAND
    *
    * ----------------------------------------------------------------------------
    *
    *     public void commandName(String[] args, MessageReceivedEvent event) {
    *         --> code here <--
    *     }
    *
    * ----------------------------------------------------------------------------
    *
    *
    */


}
