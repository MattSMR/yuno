package me.mattsmr.yuno;

import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.sedmelluq.discord.lavaplayer.player.AudioLoadResultHandler;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayer;
import com.sedmelluq.discord.lavaplayer.player.DefaultAudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.source.AudioSourceManagers;
import com.sedmelluq.discord.lavaplayer.tools.FriendlyException;
import com.sedmelluq.discord.lavaplayer.track.AudioPlaylist;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import me.mattsmr.yuno.music.AudioEvents;
import me.mattsmr.yuno.music.MusicManager;
import me.mattsmr.yuno.music.SongSelection;
import me.mattsmr.yuno.utils.BlackjackManager;
import me.mattsmr.yuno.utils.Playlist;
import me.mattsmr.yuno.utils.Trivia;
import net.dean.jraw.RedditClient;
import net.dean.jraw.http.NetworkAdapter;
import net.dean.jraw.http.OkHttpNetworkAdapter;
import net.dean.jraw.http.UserAgent;
import net.dean.jraw.models.Listing;
import net.dean.jraw.models.Submission;
import net.dean.jraw.oauth.Credentials;
import net.dean.jraw.oauth.OAuthHelper;
import net.dean.jraw.pagination.DefaultPaginator;
import net.dv8tion.jda.core.*;
import net.dv8tion.jda.core.entities.Game;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.events.ReadyEvent;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.events.message.react.MessageReactionAddEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import net.kodehawa.lib.imageboards.entities.BoardImage;
import net.kodehawa.lib.imageboards.DefaultImageBoards;
import pw.aru.api.nekos4j.Nekos4J;
import pw.aru.api.nekos4j.image.Image;
import pw.aru.api.nekos4j.image.ImageProvider;

import javax.security.auth.login.LoginException;
import java.awt.*;
import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicBoolean;

public class Yuno extends ListenerAdapter {

    public static JDA discord;
    static RedditClient reddit;
    public static DefaultAudioPlayerManager playerManager;
    public static AudioPlayer player;
    public static HashMap<String, SongSelection> selections;

    static HashMap<String, Message> trivias = new HashMap<>();
    private static HashMap<String, Trivia> questions = new HashMap<>();
    public static HashMap<String, BlackjackManager> blackjacks = new HashMap<>();

    private static String username;
    private static String password;
    private static String clientId;
    private static String clientSecret;

    public static void main(String[] args) throws IOException {
        username = System.getenv("USERNAME");
        password = System.getenv("PASSWORD");
        clientId = System.getenv("CLIENT_ID");
        clientSecret = System.getenv("CLIENT_SECRET");
        try {
            discord = new JDABuilder(AccountType.BOT).setToken(System.getenv("TOKEN")).addEventListener(new Yuno()).buildBlocking();
        } catch (LoginException | InterruptedException ex) {
            ex.printStackTrace();
        }
        playerManager = new DefaultAudioPlayerManager();
        AudioSourceManagers.registerRemoteSources(playerManager);
        player = playerManager.createPlayer();
        player.addListener(new AudioEvents());
        selections = new HashMap<>();
        Playlist.init();
        reddit = createReddit();
    }

    private static RedditClient createReddit() {
        UserAgent agent = new UserAgent("bot", "me.mattsmr.yuno", "v0.1", username);
        Credentials credentials = Credentials.script(username, password, clientId, clientSecret);
        NetworkAdapter adapter = new OkHttpNetworkAdapter(agent);
        return OAuthHelper.automatic(adapter, credentials);
    }

    @Override
    public void onReady(ReadyEvent event) {
        event.getJDA().getPresence().setPresence(OnlineStatus.ONLINE, Game.watching("habin sleep."));
    }

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        if (event.getMessage().getContentDisplay().startsWith(".")) {
            AtomicBoolean commandExists = new AtomicBoolean(false);
            String command = event.getMessage().getContentDisplay().replaceFirst(".", "");
            String commandName = command.contains(" ") ? command.split(" ")[0] : command;
            if (commandName.startsWith("_")) commandName = commandName.split("_")[1];
            String finalCommandName = commandName;
            Arrays.stream(Commands.class.getDeclaredMethods()).forEach(method -> {
                if (!commandExists.get())
                    commandExists.set(method.getName().equalsIgnoreCase(finalCommandName));
            });
            if (!commandExists.get()) return;
            try {
                Method method = Commands.class.getDeclaredMethod(commandName, String[].class, MessageReceivedEvent.class);
                method.invoke(new Commands(), command.startsWith(commandName + " ") ? command.replace(commandName + " ", "").split(" ") : ArrayUtils.EMPTY_STRING_ARRAY, event);
            } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void onMessageReactionAdd(MessageReactionAddEvent event) {
        if (event.getUser().isBot()) return;
        if (trivias.containsKey(event.getMessageId())) {
            JSONObject trivia;
            EmbedBuilder embedBuilder = new EmbedBuilder();
            if (event.getReactionEmote().getName().equalsIgnoreCase("\uD83C\uDFAC")) {
                // Film/TV
                try {
                    trivia = Unirest.get("https://opentdb.com/api.php?amount=1&category=11&type=multiple")
                            .asJson().getBody().getObject().getJSONArray("results").getJSONObject(0);
                } catch (JSONException | UnirestException ex) {
                    ex.printStackTrace();
                    return;
                }
            } else if (event.getReactionEmote().getName().equalsIgnoreCase("\uD83C\uDDEF\uD83C\uDDF5")) {
                // Anime
                try {
                    trivia = Unirest.get("https://opentdb.com/api.php?amount=1&category=31&type=multiple")
                            .asJson().getBody().getObject().getJSONArray("results").getJSONObject(0);
                } catch (JSONException | UnirestException ex) {
                    ex.printStackTrace();
                    return;
                }
            } else if (event.getReactionEmote().getName().equalsIgnoreCase("\uD83C\uDFAE")) {
                // Video Games
                try {
                    trivia = Unirest.get("https://opentdb.com/api.php?amount=1&category=15&type=multiple")
                            .asJson().getBody().getObject().getJSONArray("results").getJSONObject(0);
                } catch (JSONException | UnirestException ex) {
                    ex.printStackTrace();
                    return;
                }
            } else if (event.getReactionEmote().getName().equalsIgnoreCase("\uD83D\uDCBB")) {
                // Computer Science
                try {
                    trivia = Unirest.get("https://opentdb.com/api.php?amount=1&category=18&type=multiple")
                            .asJson().getBody().getObject().getJSONArray("results").getJSONObject(0);
                } catch (JSONException | UnirestException ex) {
                    ex.printStackTrace();
                    return;
                }
            } else if (event.getReactionEmote().getName().equalsIgnoreCase("\uD83E\uDD14")) {
                // General Knowledge
                try {
                    trivia = Unirest.get("https://opentdb.com/api.php?amount=1&category=9&type=multiple")
                            .asJson().getBody().getObject().getJSONArray("results").getJSONObject(0);
                } catch (JSONException | UnirestException ex) {
                    ex.printStackTrace();
                    return;
                }
            } else if (event.getReactionEmote().getName().equalsIgnoreCase("\uD83D\uDD00")) {
                // Random
                try {
                    trivia = Unirest.get("https://opentdb.com/api.php?amount=1&type=multiple")
                            .asJson().getBody().getObject().getJSONArray("results").getJSONObject(0);
                } catch (JSONException | UnirestException ex) {
                    ex.printStackTrace();
                    return;
                }
            } else {
                return;
            }
            try {
                event.getChannel().getMessageById(event.getMessageId()).submit().get().delete().submit();
            } catch (InterruptedException | ExecutionException ex) {
                ex.printStackTrace();
            }
            Trivia question = new Trivia();
            question.setOwner(event.getUser());
            question.setMessage(event.getMessageId());
            char[] alphabet = "abcdef".toCharArray();
            try {
                String title = trivia.getString("question");
                if (title.contains("&quot;")) {
                    title = title.replaceAll("&quot;", "\"");
                }
                if (title.contains("&#039;")) {
                    title = title.replaceAll("&#039;", "'");
                }
                embedBuilder.setTitle(title);
                embedBuilder.setColor(ThreadLocalRandom.current().nextInt());
                List<String> answers = new ArrayList<>();
                for (int i = 0; i < trivia.getJSONArray("incorrect_answers").length(); i++) {
                    if (trivia.getJSONArray("incorrect_answers").getString(i) == null) {
                        break;
                    }
                    answers.add(trivia.getJSONArray("incorrect_answers").getString(i));
                }
                answers.add(trivia.getString("correct_answer"));
                Collections.shuffle(answers);
                for (int i = 0; i < answers.size(); i++) {
                    if (answers.get(i).equalsIgnoreCase(trivia.getString("correct_answer"))) {
                        question.setAnswer(alphabet[i]);
                        break;
                    }
                }
                for (int i = 0; i < answers.size(); i++) {
                    embedBuilder.appendDescription("\n\n:regional_indicator_" + alphabet[i] + ": " + answers.get(i));
                }
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
            event.getChannel().sendMessage(embedBuilder.build()).queue(message -> {
                message.addReaction("\uD83C\uDDE6").queue();
                message.addReaction("\uD83C\uDDE7").queue();
                message.addReaction("\uD83C\uDDE8").queue();
                message.addReaction("\uD83C\uDDE9").queue();
                questions.put(message.getId(), question);
            });
        } else if (questions.containsKey(event.getMessageId())) {
            Message message = null;
            try {
                message = event.getChannel().getMessageById(event.getMessageId()).submit().get();
            } catch (InterruptedException | ExecutionException ex) {
                ex.printStackTrace();
            }
            if (message == null) return;
            if (!event.getUser().equals(questions.get(event.getMessageId()).getOwner())) return;
            if (event.getReactionEmote().getName().equalsIgnoreCase("\uD83C\uDDE6")) {
                if (questions.get(event.getMessageId()).getAnswer() == 'a') {
                    message.clearReactions().submit();
                    message.editMessage(":white_check_mark: Correct! The answer was "
                            + String.valueOf(questions.get(event.getMessageId()).getAnswer()).toUpperCase() + ".").submit();
                } else {
                    message.clearReactions().submit();
                    message.editMessage(":negative_squared_cross_mark: Incorrect! The answer was "
                            + String.valueOf(questions.get(event.getMessageId()).getAnswer()).toUpperCase() + " (You chose A).").submit();
                }
            } else if (event.getReactionEmote().getName().equalsIgnoreCase("\uD83C\uDDE7")) {
                if (questions.get(event.getMessageId()).getAnswer() == 'b') {
                    message.clearReactions().submit();
                    message.editMessage(":white_check_mark: Correct! The answer was "
                            + String.valueOf(questions.get(event.getMessageId()).getAnswer()).toUpperCase() + ".").submit();
                } else {
                    message.clearReactions().submit();
                    message.editMessage(":negative_squared_cross_mark: Incorrect! The answer was "
                            + String.valueOf(questions.get(event.getMessageId()).getAnswer()).toUpperCase() + " (You chose B).").submit();
                }
            } else if (event.getReactionEmote().getName().equalsIgnoreCase("\uD83C\uDDE8")) {
                if (questions.get(event.getMessageId()).getAnswer() == 'c') {
                    message.clearReactions().submit();
                    message.editMessage(":white_check_mark: Correct! The answer was "
                            + String.valueOf(questions.get(event.getMessageId()).getAnswer()).toUpperCase() + ".").submit();
                } else {
                    message.clearReactions().submit();
                    message.editMessage(":negative_squared_cross_mark: Incorrect! The answer was "
                            + String.valueOf(questions.get(event.getMessageId()).getAnswer()).toUpperCase() + " (You chose C).").submit();
                }
            } else if (event.getReactionEmote().getName().equalsIgnoreCase("\uD83C\uDDE9")) {
                if (questions.get(event.getMessageId()).getAnswer() == 'd') {
                    message.clearReactions().submit();
                    message.editMessage(":white_check_mark: Correct! The answer was "
                            + String.valueOf(questions.get(event.getMessageId()).getAnswer()).toUpperCase() + ".").submit();
                } else {
                    message.clearReactions().submit();
                    message.editMessage(":negative_squared_cross_mark: Incorrect! The answer was "
                            + String.valueOf(questions.get(event.getMessageId()).getAnswer()).toUpperCase() + " (You chose D).").submit();
                }
            }
        } else if (selections.containsKey(event.getMessageId())) {
            SongSelection selection = selections.get(event.getMessageId());
            JSONObject song;
            if (event.getReactionEmote().getName().equalsIgnoreCase("\uD83C\uDDE6")) {
                song = selection.getSongs().get(0);
            } else if (event.getReactionEmote().getName().equalsIgnoreCase("\uD83C\uDDE7")) {
                song = selection.getSongs().get(1);
            } else if (event.getReactionEmote().getName().equalsIgnoreCase("\uD83C\uDDE8")) {
                song = selection.getSongs().get(2);
            } else if (event.getReactionEmote().getName().equalsIgnoreCase("\uD83C\uDDE9")) {
                song = selection.getSongs().get(3);
            } else if (event.getReactionEmote().getName().equalsIgnoreCase("\uD83C\uDDEA")) {
                song = selection.getSongs().get(4);
            } else {
                return;
            }
            String url;
            try {
                url = "https://www.youtube.com/watch?v=" + song.getJSONObject("id").getString("videoId");
            } catch (JSONException ex) {
                ex.printStackTrace();
                return;
            }
            AtomicBoolean isPlaylist = new AtomicBoolean(false);
            try {
                isPlaylist.set(event.getTextChannel().getMessageById(event.getMessageId()).submit().get().getEmbeds().get(0).getTitle().equalsIgnoreCase("Playlist"));
            } catch (InterruptedException | ExecutionException ex) {
                ex.printStackTrace();
            }
            Yuno.playerManager.loadItem(url,
                    new AudioLoadResultHandler() {
                        @Override
                        public void trackLoaded(AudioTrack track) {
                            if (isPlaylist.get()) {
                                Playlist.add(event.getUser(), track);
                                event.getChannel().sendMessage(":notepad_spiral: Added `" + track.getInfo().title + "` to playlist.").submit();
                            } else {
                                MusicManager.queue.add(track);
                                try {
                                    event.getChannel().getMessageById(event.getMessageId()).submit().get()
                                            .editMessage(":notepad_spiral: **Queued:** `" + song.getJSONObject("snippet").getString("title") + "`.").submit();
                                } catch (JSONException | InterruptedException | ExecutionException ex) {
                                    ex.printStackTrace();
                                }
                                if (Yuno.player.getPlayingTrack() == null) {
                                    Yuno.player.playTrack(MusicManager.queue.get(0));
                                    try {
                                        event.getChannel().getMessageById(event.getMessageId()).submit().get()
                                                .editMessage(":radio_button: **Now playing:** `" + song.getJSONObject("snippet").getString("title") + "`.").submit();
                                    } catch (InterruptedException | ExecutionException | JSONException ex) {
                                        ex.printStackTrace();
                                    }
                                    MusicManager.queue.remove(0);
                                }
                            }
                        }

                        @Override
                        public void playlistLoaded(AudioPlaylist playlist) {
                            try {
                                MusicManager.queue.addAll(playlist.getTracks());
                                event.getChannel().getMessageById(event.getMessageId()).submit().get()
                                        .editMessage(":notepad_spiral: **Queued playlist.**").submit();
                            } catch (InterruptedException | ExecutionException ex) {
                                ex.printStackTrace();
                            }
                        }

                        @Override
                        public void noMatches() {
                            try {
                                event.getChannel().getMessageById(event.getMessageId()).submit().get()
                                        .editMessage("i couldn't find anything!! >///<").submit();
                            } catch (InterruptedException | ExecutionException ex) {
                                ex.printStackTrace();
                            }
                            event.getGuild().getAudioManager().closeAudioConnection();
                        }

                        @Override
                        public void loadFailed(FriendlyException throwable) {
                            try {
                                event.getChannel().getMessageById(event.getMessageId()).submit().get()
                                        .editMessage("everything just exploded omg what did you do???").submit();
                            } catch (InterruptedException | ExecutionException ex) {
                                ex.printStackTrace();
                            }
                            event.getGuild().getAudioManager().closeAudioConnection();
                        }
                    }
            );
            try {
                event.getChannel().getMessageById(event.getMessageId()).submit().get().clearReactions().submit();
            } catch (InterruptedException | ExecutionException ex) {
                ex.printStackTrace();
            }
        } else if (blackjacks.containsKey(event.getMessageId())) {
            if (event.getReactionEmote().getName().equalsIgnoreCase("\uD83C\uDDF8")) {
                // Stand
                event.getChannel().sendMessage("You stood.").submit();
                try {
                    blackjacks.get(event.getMessageId()).stand();
                } catch (UnirestException ex) {
                    ex.printStackTrace();
                }
            } else if (event.getReactionEmote().getName().equalsIgnoreCase("\uD83C\uDDED")) {
                // Hit
                event.getChannel().sendMessage("You hit.").submit();
                try {
                    blackjacks.get(event.getMessageId()).hit();
                } catch (UnirestException | ExecutionException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    private static String findClosestMatch(Collection<String> collection, String target) {
        int distance = 3;
        String closest = null;
        for (String data : collection) {
            int currentDistance = StringUtils.getLevenshteinDistance(data, target);
            if (currentDistance < distance) {
                distance = currentDistance;
                closest = data;
            }
        }
        return closest;
    }

}
