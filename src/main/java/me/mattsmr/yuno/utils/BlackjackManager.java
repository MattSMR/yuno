package me.mattsmr.yuno.utils;

import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import me.mattsmr.yuno.Yuno;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import org.json.JSONArray;
import org.json.JSONObject;

import java.awt.*;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class BlackjackManager {

    private String id;
    private MessageReceivedEvent event;
    private List<JSONObject> userCards;
    private List<JSONObject> dealerCards;
    private Message message;

    public BlackjackManager(MessageReceivedEvent event) throws UnirestException {
        JSONObject deck;
        deck = Unirest.get("https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1").asJson().getBody().getObject();
        if (!deck.getBoolean("success")) {
            event.getChannel().sendMessage("something just blew up. try again later.").submit();
            return;
        }
        setId(deck.getString("deck_id"));
        setEvent(event);
    }

    private String getId() {
        return id;
    }

    private void setId(String id) {
        this.id = id;
    }

    public void init() throws UnirestException, ExecutionException {
        JSONObject draw = Unirest.get("https://deckofcardsapi.com/api/deck/" + getId() + "/draw/?count=4").asJson().getBody().getObject();
        JSONArray cards = draw.getJSONArray("cards");
        userCards = Stream.of(cards.getJSONObject(0), cards.getJSONObject(1)).collect(Collectors.toList());
        dealerCards = Stream.of(cards.getJSONObject(2), cards.getJSONObject(3)).collect(Collectors.toList());
        EmbedBuilder embedBuilder = new EmbedBuilder();
        embedBuilder.setDescription("**Your Cards:**");
        userCards.forEach(card -> embedBuilder.appendDescription("\n - `" + card.getString("value") + " of " + card.getString("suit") + "`"));
        embedBuilder.appendDescription("\n\n**Dealer's Cards:**");
        if (appendDealerCards(embedBuilder)) return;
        embedBuilder.setColor(Color.ORANGE);
        embedBuilder.setFooter("React with `H` to hit or `S` to stand.", cards.getJSONObject(0).getString("image"));
        try {
            if (message != null) message.delete().submit();
            message = event.getChannel().sendMessage(embedBuilder.build()).submit().get();
            Yuno.blackjacks.put(message.getId(), this);
            message.addReaction("\uD83C\uDDED").queue();
            message.addReaction("\uD83C\uDDF8").queue();
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }

    public void hit() throws UnirestException, ExecutionException {
        message.delete().submit();
        EmbedBuilder embedBuilder = new EmbedBuilder();
        embedBuilder.setDescription("**Your Cards:**");
        JSONObject draw = Unirest.get("https://deckofcardsapi.com/api/deck/" + getId() + "/draw/?count=1").asJson().getBody().getObject().getJSONArray("cards").getJSONObject(0);
        userCards.add(draw);
        userCards.forEach(card -> embedBuilder.appendDescription("\n - `" + card.getString("value") + " of " + card.getString("suit") + "`"));
        embedBuilder.appendDescription("\n\n**Dealer's Cards:**");
        if (appendDealerCards(embedBuilder)) return;
        embedBuilder.setColor(Color.ORANGE);
        AtomicInteger userTotal = new AtomicInteger(0);
        userCards.forEach(card -> editTotal(userTotal, card));
        if (userTotal.get() > 21) {
            event.getChannel().sendMessage(event.getAuthor().getAsMention() + " bust.\nThe dealer wins!").submit();
            return;
        }
        embedBuilder.setFooter("React with `H` to hit or `S` to stand.", userCards.get(0).getString("image"));
        try {
            Yuno.blackjacks.remove(message.getId());
            message = event.getChannel().sendMessage(embedBuilder.build()).submit().get();
            Yuno.blackjacks.put(message.getId(), this);
            message.addReaction("\uD83C\uDDED").queue();
            message.addReaction("\uD83C\uDDF8").queue();
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
        final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
        executorService.scheduleAtFixedRate(() -> {
            Yuno.blackjacks.remove(message.getId());
            message.delete().submit();
            EmbedBuilder newEmbed = new EmbedBuilder();
            newEmbed.setDescription("**Your Cards:**");
            try {
                userCards.add(Unirest.get("https://deckofcardsapi.com/api/deck/" + getId() + "/draw/?count=1").asJson().getBody().getObject());
            } catch (UnirestException ex) {
                ex.printStackTrace();
            }
            userCards.forEach(card -> newEmbed.appendDescription("\n - `" + card.getString("value") + " of " + card.getString("suit") + "`"));
            newEmbed.appendDescription("\n\n**Dealer's Cards:**");
            try {
                dealerCards.add(Unirest.get("https://deckofcardsapi.com/api/deck/" + getId() + "/draw/?count=1").asJson().getBody().getObject());
            } catch (UnirestException ex) {
                ex.printStackTrace();
            }
            if (appendDealerCards(newEmbed)) return;
            newEmbed.setColor(Color.ORANGE);
            newEmbed.setFooter("React with `H` to hit or `S` to stand.", userCards.get(0).getString("image"));
            try {
                message = event.getChannel().sendMessage(newEmbed.build()).submit().get();
                message.addReaction("\uD83C\uDDED").queue();
                message.addReaction("\uD83C\uDDF8").queue();
                Yuno.blackjacks.put(message.getId(), this);
            } catch (InterruptedException | ExecutionException ex) {
                ex.printStackTrace();
            }
        }, 0, 3, TimeUnit.SECONDS);
    }

    private boolean appendDealerCards(EmbedBuilder embedBuilder) {
        for (int i = 0; i < dealerCards.size(); i++) {
            if (i - 1 == dealerCards.size()) {
                embedBuilder.appendDescription("\n - `???`");
                return true;
            }
            embedBuilder.appendDescription("\n - `" + dealerCards.get(i).getString("value") + " of " + dealerCards.get(i).getString("suit") + "`");
        }
        return false;
    }

    public void stand() throws UnirestException {
        EmbedBuilder newEmbed = new EmbedBuilder();
        newEmbed.setDescription("**Your Cards:**");
        userCards.forEach(card -> newEmbed.appendDescription("\n - `" + card.getString("value") + " of " + card.getString("suit") + "`"));
        newEmbed.appendDescription("\n\n**Dealer's Cards:**");
        AtomicInteger dealerTotal = new AtomicInteger(0);
        dealerCards.forEach(card -> editTotal(dealerTotal, card));
        AtomicInteger userTotal = new AtomicInteger(0);
        userCards.forEach(card -> editTotal(userTotal, card));
        if (appendDealerCards(newEmbed)) return;
        while (userTotal.get() >= dealerTotal.get()) {
            if (dealerTotal.get() > 21) {
                event.getChannel().sendMessage("The dealer bust.\n" + event.getAuthor().getAsMention() + " wins!").submit();
                return;
            }
            JSONObject card = Unirest.get("https://deckofcardsapi.com/api/deck/" + getId() + "/draw/?count=1").asJson().getBody().getObject();
            dealerCards.add(card);
            editTotal(dealerTotal, card);
        }
        newEmbed.setColor(Color.ORANGE);
        try {
            message = event.getChannel().sendMessage(newEmbed.build()).submit().get();
            if (userTotal.get() > dealerTotal.get()) {
                event.getChannel().sendMessage(event.getAuthor().getAsMention() + " wins!").submit();
            } else {
                event.getChannel().sendMessage("The dealer wins!").submit();
            }
        } catch (InterruptedException | ExecutionException ex) {
            ex.printStackTrace();
        }
        Yuno.blackjacks.remove(message.getId());
        message.delete().submit();
    }

    private void editTotal(AtomicInteger total, JSONObject card) {
        if (card.getString("value").equalsIgnoreCase("KING") || card.getString("value").equalsIgnoreCase("QUEEN") || card.getString("value").equalsIgnoreCase("JACK")) {
            total.addAndGet(10);
        } else if (card.getString("value").equalsIgnoreCase("ACE")) {
            total.addAndGet(11); // TODO offer choice of one or eleven...
        } else {
            total.addAndGet(Integer.parseInt(card.getString("value")));
        }
    }

    private void setEvent(MessageReceivedEvent event) {
        this.event = event;
    }

}
