package me.mattsmr.yuno.utils;

import net.dv8tion.jda.core.entities.User;

public class Trivia {

    private User owner;
    private String message;
    private char answer;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public char getAnswer() {
        return answer;
    }

    public void setAnswer(char answer) {
        this.answer = answer;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

}
