package me.mattsmr.yuno.utils;

import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import me.mattsmr.yuno.music.MusicManager;
import net.dv8tion.jda.core.entities.User;
import org.apache.commons.configuration2.YAMLConfiguration;
import org.apache.commons.configuration2.builder.FileBasedConfigurationBuilder;
import org.apache.commons.configuration2.builder.fluent.Parameters;
import org.apache.commons.configuration2.builder.fluent.PropertiesBuilderParameters;
import org.apache.commons.configuration2.convert.DefaultListDelimiterHandler;
import org.apache.commons.configuration2.convert.ListDelimiterHandler;
import org.apache.commons.configuration2.ex.ConfigurationException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Playlist {

    private static YAMLConfiguration config;

    public static void init() throws IOException {
        File file = new File("playlists.yml");
        file.createNewFile();
        ListDelimiterHandler delimiter = new DefaultListDelimiterHandler('-');
        PropertiesBuilderParameters propertyParameters = new Parameters().properties();
        propertyParameters.setFile(file);
        propertyParameters.setThrowExceptionOnMissing(true);
        propertyParameters.setListDelimiterHandler(delimiter);
        FileBasedConfigurationBuilder<YAMLConfiguration> builder = new FileBasedConfigurationBuilder<>(YAMLConfiguration.class);
        builder.configure(propertyParameters);
        builder.setAutoSave(true);
        try {
            config = builder.getConfiguration();
        } catch (ConfigurationException ex) {
            ex.printStackTrace();
        }
    }

    public static void add(User user, AudioTrack track) {
        List<String> identifiers = config.containsKey(user.getId()) ? config.getList(String.class, user.getId()) : new ArrayList<>();
        identifiers.add(track.getIdentifier());
        config.setProperty(user.getId(), identifiers);
    }

    public static List<AudioTrack> get(User author) {
        List<String> identifiers = config.containsKey(author.getId()) ? config.getList(String.class, author.getId()) : new ArrayList<>();
        List<AudioTrack> tracks = new ArrayList<>();
        identifiers.forEach(identifier -> tracks.add(MusicManager.getTrackFromIdentifier("https://www.youtube.com/watch?v=" + identifier)));
        return tracks;
    }

    public static void reset(User author) {
        config.setProperty(author.getId(), new ArrayList<>());
    }
}
